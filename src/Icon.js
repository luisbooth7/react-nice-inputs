import React, { Component } from 'react';
import propTypes from 'prop-types';
import utils from './utils';

class Icon extends Component {
  state = {
    classList: ''
  }

  componentDidMount() {
    this.prepareClassList();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.classList.length < this.props.classList.length || utils.checkClassListNames(prevProps.classList, this.props.classList)) {
      this.prepareClassList();
    }
  }

  prepareClassList = () => {
    const { classList } = this.props;
    let classListString = utils.createClassList(classList);

    classListString = `icon-aid ${ classListString }`;

    this.setState({ classList: classListString });
  }

  render() {
    const { classList } = this.state;

    return (
      <i className={ classList } />
    );
  }
}

Icon.propTypes = {
  classList: propTypes.array
};

Icon.defaultProps = {
  classList: []
};

export default Icon;

'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _utils = require('./utils');

var _utils2 = _interopRequireDefault(_utils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Input = function (_Component) {
  _inherits(Input, _Component);

  function Input(props) {
    _classCallCheck(this, Input);

    // create a ref to store the thisInput DOM element
    var _this = _possibleConstructorReturn(this, (Input.__proto__ || Object.getPrototypeOf(Input)).call(this, props));

    _this.state = {
      classList: '',
      fill: '*****',
      what2show: '',
      maskValue: '',
      inputValue: ''
    };

    _this.prepopulateInput = function () {
      var _this$props = _extends({}, _this.props),
          value = _this$props.value,
          name = _this$props.name;

      var valAsStr = value ? '' + value : '';

      if (value && valAsStr !== '') {
        _this.setState({ inputValue: valAsStr, maskValue: valAsStr }, function () {
          _this.mask();
          _this.setState({ maskValue: _this.state.what2show }, function () {
            _this.onBlur();
            _this.handleChange(value, name, _this.thisInput.current);
          });
        });
      }
      // if (typeof value === 'string' && valAsStr === '') {
      //   this.setState({ inputValue: '', maskValue: '', what2show: '' }, () => {
      //     const thisInput = this.thisInput.current;

      //     this.handleChange(value, name, thisInput);
      //   });
      // }
    };

    _this.prepareClassList = function () {
      var _this$props2 = _this.props,
          classList = _this$props2.classList,
          isValid = _this$props2.isValid,
          isInvalid = _this$props2.isInvalid;

      var classListString = _utils2.default.createClassList(classList, isValid, isInvalid);

      classListString = 'input ' + classListString;

      _this.setState({ classList: classListString });
    };

    _this.handleMaskChange = function (e) {
      var el = e.target;
      var value = el.value;
      var thisInput = _this.thisInput.current;
      var name = _this.props.name;


      _this.setState({ inputValue: value, maskValue: value }, function () {
        _this.handleChange(value, name, thisInput);
        _this.mask();
        // this.workFormat();
      });
    };

    _this.handleChange = function (value, name, e) {
      _this.props.onChange(value, name, e);
    };

    _this.workFormat = function () {
      var cc = _this.props.cc;


      if (cc && _this.state.inputValue.length > 0 && _this.state.inputValue.length < 17) {
        var maskValue = _this.state.inputValue.match(new RegExp('.{1,4}', 'g')).join('-');

        console.log(maskValue);

        // this.setState({ maskValue });
      }
    };

    _this.onBlur = function () {
      // get mask here and POP mask it
      var isInvalid = _this.props.isInvalid;

      var negatedValidity = isInvalid || false;
      var finalValue = negatedValidity ? _this.state.maskValue : _this.state.what2show;

      _this.setState({ maskValue: finalValue });
    };

    _this.onFocus = function () {
      // clear both inputs
      _this.setState({ inputValue: '', maskValue: '', what2show: '' }, function () {
        var thisInput = _this.thisInput.current;
        var name = _this.props.name;
        var value = _this.state.inputValue;


        _this.handleChange(value, name, thisInput);
      });
    };

    _this.mask = function () {
      var inputValue = _this.state.inputValue;
      var fill = _this.state.fill;
      var _this$props3 = _this.props,
          mask = _this$props3.mask,
          last = _this$props3.last,
          type = _this$props3.type,
          cc = _this$props3.cc;

      var regex = new RegExp(mask || '.*', 'i');

      // if (inputValue === '') { return false; }
      if (inputValue.indexOf(fill) === -1 && regex.test(inputValue)) {
        // Matches the regex? else put blank
        var what2show = inputValue.length !== inputValue.match(regex)[0].length ? inputValue.match(regex)[0] : '';

        // const ccrep = inputValue.replace(/\d(?=\d{4})/g, '*');

        // console.log(ccrep);

        // Replace start or end?
        what2show = last ? '' + fill + what2show : '' + what2show + fill;

        // is type email? mask but show first letter
        what2show = type === 'email' ? '' + inputValue[0] + fill + what2show.replace(fill, '') : what2show;

        // is CC? show last 4 digits
        what2show = cc ? inputValue.replace(/\d(?=\d{4})/g, '*') : what2show;

        _this.setState({ what2show: what2show });
      } else {
        _this.setState({ what2show: inputValue });
      }
    };

    _this.thisInput = _react2.default.createRef();
    _this.thisMask = _react2.default.createRef();
    return _this;
  }

  _createClass(Input, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.prepareClassList();
      this.prepopulateInput();
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps) {
      if (prevProps.classList && this.props.classList && prevProps.classList.length < this.props.classList.length || _utils2.default.checkClassListNames(prevProps.classList, this.props.classList)) {
        this.prepareClassList();
      }

      if (prevProps.hasOwnProperty('value') && this.props.hasOwnProperty('value')) {
        if (prevProps.value !== this.props.value) {
          this.prepopulateInput();
        }
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _props = this.props,
          name = _props.name,
          attrs = _props.attrs;
      var _state = this.state,
          classList = _state.classList,
          inputValue = _state.inputValue,
          maskValue = _state.maskValue;

      var type = 'text';

      return _react2.default.createElement(
        _react.Fragment,
        null,
        _react2.default.createElement('input', { ref: this.thisInput, type: 'hidden', name: name, id: name, value: inputValue, onChange: this.handleChange }),
        _react2.default.createElement('input', _extends({ ref: this.thisMask, type: type, name: name, className: classList, onChange: this.handleMaskChange, onFocus: this.onFocus, onBlur: this.onBlur }, attrs, { value: maskValue }))
      );
    }
  }]);

  return Input;
}(_react.Component);

Input.propTypes = {
  type: _propTypes2.default.string.isRequired,
  name: _propTypes2.default.string.isRequired,
  classList: _propTypes2.default.array.isRequired,
  mask: _propTypes2.default.string,
  cc: _propTypes2.default.bool,
  attrs: _propTypes2.default.object,
  onChange: _propTypes2.default.func.isRequired,
  isValid: _propTypes2.default.bool,
  isInvalid: _propTypes2.default.bool,
  value: _propTypes2.default.string,
  last: _propTypes2.default.any
};

exports.default = Input;